package com.darkshogun;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.cxf.common.message.CxfConstants;
import javax.xml.transform.TransformerFactory;

import java.io.*;

/**
 * User: grimm
 * Date: 3/19/13
 * Time: 4:41 PM
 */
public class SampleRoute extends RouteBuilder {
    private String SERVICE_URI = "cxfrs://http://localhost:8989/?resourceClasses=com.darkshogun.SampleService";
    private enum xmlTypes {
        VALID("xml/valid.xml");

        private String type;
        xmlTypes(String s){
           this.type = s;
        }
        @Override
        public String toString(){
            return type;
        }
    };
    @Override
    public void configure() throws Exception {
        Processor loadXml = new Processor() {
            @Override
            public void process(Exchange exchange) throws Exception {
                InputStream is = getClass().getResourceAsStream((String) exchange.getIn().getHeader("xmlType"));
                BufferedReader bf = new BufferedReader(new InputStreamReader(is));
                String xml = "";
                while(bf.ready())
                    xml+= bf.readLine();
                bf.close();
                exchange.getIn().setBody(xml);
                exchange.getOut().setBody(xml);

            }
        };
        interceptFrom("cxfrs:*").routeId("clown404n").
                log("Caught a valid one.")
                .choice()
                  .when(header(CxfConstants.OPERATION_NAME).isEqualTo("valid")).setHeader("xmlType", constant("/xml/valid.xml"));
        from(SERVICE_URI).log("HelloWorld !!!").
        process(loadXml).
                doTry().
                to("validator:xsd/validate.xsd").
                to("xslt:xsl/validator.xsl").
                doCatch(org.apache.camel.ValidationException.class).
                process(new Processor(){
                    @Override
                    public void process(Exchange exchange) throws Exception {
                        log.error(exchange.getProperty(Exchange.EXCEPTION_CAUGHT).toString());
                    }
                }).end().
                end();
    }
}
