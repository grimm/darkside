package com.darkshogun;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.ByteArrayInputStream;
import java.io.StringBufferInputStream;
import java.net.URL;

/**
 * Camel Component that performs schema validation with multiple xsd sources.
 *
 * As per documentation camels default validator has that restriction.
 *
 * Has been deprecated in favor of a simple approach with camel validator component.
 * User: grimm
 * Date: 3/20/13
 * Time: 4:52 PM
 */
@Deprecated
public class XMLSchemaValidator implements Processor {

    @Override
    public void process(Exchange exchange) throws Exception {
        Source xmlFile = new StreamSource(new ByteArrayInputStream(exchange.getIn().getBody().toString().getBytes("UTF-8")));
        Source hrxml = new StreamSource(getClass().getResource("/xsd/hrxml.xsd").toString());
        Source iqn = new StreamSource(getClass().getResource("/xsd/iqnavigator.xsd").toString());
        SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);

        Validator validator = schemaFactory.newSchema(new Source[] {hrxml, iqn}).newValidator();
        try {
            validator.validate(xmlFile);
            exchange.getOut().setBody("The XML is valid");
        }catch (SAXException e){
            exchange.getOut().setBody(e.getLocalizedMessage());
        }
    }
}
