<?xml version="1.0" encoding="UTF-8"?>
<schema xmlns="http://purl.oclc.org/dsdl/schematron" >
    <title>Schematron Validation</title>
    <!-- The following defin namespaces local to the schematron validation -->
    <ns prefix="iqn" uri="http://www.iqnavigator.com/api/1"/>
    <ns prefix="hrxml" uri="HR-XML"/>

    <!-- A schematron rule requiring the jobAttribute from our iqnavigator schema within the UserArea -->
    <pattern>
        <rule context="hrxml:assignment/hrxml:UserArea">
            <assert test="iqn:assignmentAttribute">
                A job attribute is required within
                <name/>
            </assert>
        </rule>
    </pattern>
</schema>
