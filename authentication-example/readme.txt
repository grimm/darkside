---- Authenticated Service Example ----

This Example walks you through utilizing spring-security without touching spring dm beans and context.
There are two bundles to accomplish this, one that is exposing an osgi user service and the other
that actually implements the outfacing service.

--Installation--
Install the feature authentication-exmaple
Then run commands against it from the terminal such as

grimm@inferno:~$ curl -v -u guest:123 http://localhost:9002/v1

Which should deny you access.

grimm@inferno:~$ curl -v -u admin:123 http://localhost:9002/v1

Which will grant access and return Hello World !!!


