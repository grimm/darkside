package com.darkshogun;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.log4j.Logger;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.codec.Base64;

import javax.security.auth.Subject;

/**
 * This prcoessor transforms HTTP basic auth credentials into spring security credentials.
 * A necessary process since only csf endpoint supports those by default, and cxfrs does not.
 * User: grimm
 * Date: 4/6/13
 * Time: 3:50 PM
 */
public class AuthenticationProcessor implements Processor {
    Logger log = Logger.getLogger(AuthenticationProcessor.class.getName());
    public void process(Exchange exchange) throws Exception {
        //Authorization comes in the form of "Basic Base64" therefore to decode we must split first.
        String input = exchange.getIn().getHeader("Authorization", String.class);
        if(input == null)
            return;
        input = input.split(" ")[1];
        //Once decoded basic authentication is in the form of "username:pass" therefore it must be split again.
        // http://en.wikipedia.org/wiki/Basic_access_authentication
        String userpass = new String(Base64.decode(input.getBytes()));
        log.error(userpass);
        String[] tokens= userpass.split(":");
        if(tokens.length < 2)
            return;

        // create an Authentication object
        UsernamePasswordAuthenticationToken authToken = new UsernamePasswordAuthenticationToken(tokens[0], tokens[1]);
        exchange.getIn().removeHeader("Authorization");

        // wrap it in a Subject
        Subject subject = new Subject();
        subject.getPrincipals().add(authToken);

        //Using this method we can retain the credentials to possibly share between mulitple routes
//        exchange.getIn().setHeader(Exchange.AUTHENTICATION, subject);

        // With this method credentials only last in the current thread.
        // However for restful services this is plenty since authentication only matters for entry.
        SecurityContextHolder.getContext().setAuthentication(authToken);
    }

}
