package com.darkshogun;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;

/**
 * This service is secured through spring-security.
 * User: grimm
 * Date: 4/2/13
 * Time: 9:47 PM
 */

@Path("/")
public class ExampleSecureService {

    /**
     * This is an example action.  This method is never called only used for rs service construction.
     */
    @GET
    @Path("/v1/")
    public void getString( ){}
}
