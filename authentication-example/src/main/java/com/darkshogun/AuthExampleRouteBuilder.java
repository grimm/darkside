package com.darkshogun;

import org.apache.camel.CamelAuthorizationException;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.springframework.security.access.AccessDeniedException;


/**
 * This file builds the route with error handling.
 * Resulting route is of the following form
 * Request-> parse credentials->authorize->Reply
 * During the authorization step if we encounter an exception we catch with a global handler.
 * User: grimm
 * Date: 4/4/13
 * Time: 2:14 AM
 */
public class AuthExampleRouteBuilder extends RouteBuilder {
    private Processor authenticationProcessor;

    public AuthExampleRouteBuilder() {
    }
    @Override
    public void configure() throws Exception {
        onException(CamelAuthorizationException.class)
                .handled(true)
                .setHeader(Exchange.HTTP_RESPONSE_CODE)
                    .constant(401).end()
                .setBody().constant("Access is totally Denied")
                .to("log:ERROR?showHeaders=true&multiline=true")
                .end();
        from("cxfrs://http://127.0.0.1:9002/?resourceClasses=com.darkshogun.ExampleSecureService")
        .process(getAuthenticationProcessor())
        .policy("my-policy")
            .setBody(constant("Hello World!!!!"))
            .to("log:request?showHeaders=true&multiline=true")
        .end();


    }

    public Processor getAuthenticationProcessor() {
        return authenticationProcessor;
    }

    public void setAuthenticationProcessor(Processor authenticationProcessor) {
        this.authenticationProcessor = authenticationProcessor;
    }
}
