========
Overview
========

DarkSide represents the darker side of my computer exploration, and showcases proof of concept systems developed on top
of ESB [1]_.  So far i am presenting the following  concept projects.

    * Schema validation with schematron
    * Spring-Security and blueprint integration for securing restful services.


.. [1] ESB=> Enterprise Service Bus.