package com.darkshogun.security;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Sample implementation of a user.
 * The username can be anything however the password is hardset to always be 123.
 * User: grimm
 * Date: 4/4/13
 * Time: 12:02 AM
 */
public class ServiceUser implements UserDetails {
    Collection<UserRole> roles = new ArrayList<UserRole>();
    String username;

    public void setUsername(String uname) {
        username = uname;
    }

    public void addRole(UserRole ur) {
        roles.add(ur);
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return roles;
    }

    @Override
    public String getPassword() {
        return "123";
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
