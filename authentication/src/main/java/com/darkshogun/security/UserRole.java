package com.darkshogun.security;

import org.springframework.security.core.GrantedAuthority;

/**
 * Meant to hold information about what a user is allowed to do.
 * User: grimm
 * Date: 4/4/13
 * Time: 12:07 AM
 */
public class UserRole implements GrantedAuthority {
    private String role;

    public UserRole(String role){
        this.role = role;
    }

    @Override
    public String getAuthority() {
        return role;
    }
}
