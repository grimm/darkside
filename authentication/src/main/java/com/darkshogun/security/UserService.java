package com.darkshogun.security;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.provisioning.UserDetailsManager;

/**
 * Sample implementation of a UserDetailsManager
 * This class represents a DAO for generic user information.
 * User: grimm
 * Date: 4/3/13
 * Time: 11:30 PM
 */
public class UserService  implements UserDetailsManager {

    @Override
    public void createUser(UserDetails user) {
    }

    @Override
    public void updateUser(UserDetails user) {
    }

    @Override
    public void deleteUser(String username) {
    }

    @Override
    public void changePassword(String oldPassword, String newPassword) {
    }

    /**
     * Only 2 users exist, the admin or a guest.
     * @param username
     * @return
     */
    @Override
    public boolean userExists(String username) {
        if (username.equals("admin"))
            return true;
        else if (username.equals("guest"))
            return true;
        return false;
    }

    /**
     * Two roles exist ROLE_ADMIN or ROLE_USER,
     * admin user is the only to have both.
     * @param username
     * @return
     * @throws UsernameNotFoundException
     */
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        ServiceUser user = new ServiceUser();
        user.setUsername(username);

        if(username.equals("admin")){
            user.addRole(new UserRole("ROLE_ADMIN"));
        }
        user.addRole(new UserRole("ROLE_USER"));
        return user;
    }
}
